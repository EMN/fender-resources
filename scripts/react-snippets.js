// React Snippets Good to know

// Removes need for using "this.props" in front of properties. Works PER render function only
const { pageHeaderTitle, pageHeaderSubTitle, pageHeaderBgImg, pageHeaderBgVideo } = this.props;


// Conditional inline in render function
{ this.props.pageHeaderMediaPlayerTextLine1 && pageHeaderMediaPlayerTextLine2 && 
 this.props.pageHeaderMediaPlayerLink &&
 <aside className="play-wrapper">
  <span>{ this.props.pageHeaderMediaPlayerTextLine1 }</span>
   <button className="playbtn">
    <i className="icon icon-play-triangle"></i>
   </button>
   <button className="pausebtn">
     <span className="line"></span>
     <span className="line"></span>
   </button>
   <span>{ this.props.pageHeaderMediaPlayerTextLine2 }</span>
 </aside>
}

{ this.props.pageHeaderBgVideo &&
  <div className="video-wrapper">
    <video controls="false" autoplay>
      <srsdlkfsdf/>
    </video>
  </div>
}