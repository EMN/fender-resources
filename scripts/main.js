// Default Scripts to copy over

        // Toggle Navigation
        function toggleNavBar() {
          $('.menu-nav').slideToggle(400);
        }
        $('.menu-toggle').click( toggleNavBar );

        // Form Select
        $("select").each(function() {
          $(this).wrapAll("<div class='select-wrapper'></div>");
        });

        // Responsive Tables
        $('table').addClass('table');

        $(".table").each(function(){
            $(this).wrapAll("<div class='table-wrapper'></div>");
        });

        $("table.table").click(function(){
          $(".table-wrapper").toggleClass( $(this).attr("class") );
        });

        // Responsive Videos
        $("iframe").each(function(){
          $(this).wrapAll("<div class='video-container'></div>");
        });

// Handy Scripts for Future Reference

         // Accordion
        $('.accordion-toggle').click(function(e) {
          e.preventDefault();

          var $this = $(this);
          var $thisParent = $(this).closest('li');

          if ($this.next().hasClass('show')) {
              $this.next().removeClass('show');
              $this.removeClass('switch-icon');
              $this.next().slideUp(350);
          } else {
              $this.parent().parent().find('li .accordion-content').removeClass('show');
              $this.parent().parent().find('li .accordion-toggle').removeClass('switch-icon');
              $this.parent().parent().find('li .accordion-content').slideUp(350);
              $this.next().toggleClass('show');
              $this.toggleClass('switch-icon');
              $this.next().slideToggle(350);
          }
        });

        // Docks nav to top when scrolling
        var sticky_navigation_offset_top = $('.banner').offset().top;

        var sticky_navigation = function() {
          $('body').toggleClass('traveling', $(window).scrollTop() > 0);
        };

        $(window).scroll( sticky_navigation );
        sticky_navigation();

        // Toggle Side Navigation
        $( "#show-left" ).click(function() {
          $( "#content-viewport" ).toggleClass('left-me mobile-left');
        });

        // Docks interior nav to left when scrolling
        function travelingNav() {
          $('.document').toggleClass('traveling', $(window).scrollTop() > 255 );
        }
        $(window).scroll(travelingNav);
        travelingNav();

        // Add Icon to Div Example
        $('form.search-form').find('.search-submit').append("<i class='ico ico-search ico-4x'></i>");

        // Dropdown Navigation
        $( ".dropdown-toggle" ).click(function(e) {
          e.preventDefault();

          $(this).siblings('.dropdown-menu').toggleClass('menu');

          if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).siblings('.dropdown-menu').removeClass('open-dropdown');
          }
          else {
            $('.open').removeClass('open');
            $(this).addClass('open');
            $(this).siblings('.dropdown-menu').addClass('open-dropdown');
          }

          if (document.documentElement.clientWidth < 768) {
            $(this).siblings('.dropdown-menu').slideToggle(400);
          }
        });

        // Loading an HTML page through ajax on click
        $('#search-form').on('submit', function(e){
          console.log("submit submitted");
          e.preventDefault();
          $('#results').load("/search/ajax-results");
        });

        // Disabling Google Maps zoom when scrolling through page
        $('.googlemap').click(function () {
          $('.googlemap iframe').css("pointer-events", "auto");
        });

        $('.googlemap').mouseleave(function() { 
          $('.googlemap iframe').css("pointer-events", "none"); 
        });

        // Clicking outside of div 
        $(document).mouseup(function(e)
        {
            var container = $(".menu-nav .navbar-search");
            if (!container.is(e.target) && container.has(e.target).length === 0) 
            {
              $(".menu-nav").removeClass("slide-search");
            }
        });

        // Clicking outside of div with a toggle class (can also click on link to close)
        container.on('click', function(e){
         e.preventDefault();
         $(this).toggleClass('open-filter');
        });

        $(document).mouseup(function (e){
           if (!container.is(e.target) && container.has(e.target).length === 0)
           {
              container.removeClass('open-filter');
           }
        });

        //Pages without a Page Header Background Image
        // What this does is take a look at the page header element - 
        // checks if it has an inline style, if so, it also checks if 
        // that inline style contains ‘background’, and if both of those things are true, it adds the class
        if($('.page-header').attr('style') && $('.page-header').attr('style').indexOf('background') !== -1) {
          $('body').addClass('bg-img');
        }

        // Setting active class on anchor link for One pager nav
        function scrollSpy() {
          var current;
          var scroll = $(window).scrollTop();
          $('.section').each(function(){ //each major section with anchor ID
            var $this = $(this);
            if( $this.offset().top < scroll + 120 ) { //120 is adding padding to accomodate fixed header (amount will vary depending on height of header)
              $current = $this;
            }
          });
          if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            $current = $('.section#contact-us'); //This is for when the page isn't long enough for last item
          }
          $('.home-header li a').removeClass('nav-active');
          var navItem = $('.home-header li a[href="#' + $current.attr('id') + '"]');
          navItem.addClass('nav-active');
        }

        $(window).scroll(scrollSpy);
        scrollSpy();


        // Simple, Lovely Parallax effect (Example Sites: Greenleaf (also Fairsearch and PEGCC) )
        function positionHeader() {
          // In the CSS give the ".wrapper" the following styles:
              // position:fixed;
              // z-index: 1;
              // top:0;
              // width:100%;
              // height:whatever you want the height to be;
          // In the CSS, wrap the rest of the page (not including ".wrapper") in
          // a div: "body-content-background" and set the CSS with the following style:
              // z-index: 10;
          $('.wrapper .text-that-moves-in-wrapper').css({
            '-webkit-transform': 'translateY(-' + ($(window).scrollTop() * 0.5) + 'px)',
            '-moz-transform': 'translateY(' + $(window).scrollTop() + 'px)',
            '-ms-transform': 'translateY(' + $(window).scrollTop() + 'px)',
            '-o-transform': 'translateY(' + $(window).scrollTop() + 'px)',
            'transform': 'translateY(-' + ($(window).scrollTop() * 0.5) + 'px)'
          });
        }
        $(window).scroll( positionHeader );
        positionHeader();


        // Going to anchor tag from another page
        if( window.location.hash.indexOf('#tab-') !== -1 ) {
          $(window).scrollTo('.expertise', { duration: 333, offset: -97 });
        }

        // Going to an anchor tag from another page and at a certain width
        if( $(window).width() > 480 && window.location.hash.indexOf('#tab-') !== -1 ) {
          $(window).scrollTo('.expertise', { duration: 333, offset: -97 });
        }

        // Smooth Scrolling Link
        $('.smooth-scroll').on('click', function(){
          $('html, body').animate({scrollTop: $(this.hash).offset().top}, 500);
          return false;
        });

        // SUCCESSFULL Smooth Scrolling from another page
        var offsetSize = $(".banner").innerHeight();
        $('html, body').hide();

        if (window.location.hash) {
            setTimeout(function() {
                $('html, body').scrollTop(0).show();
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top-offsetSize
                    }, 1000);
            }, 0);
        }
        else {
            $('html, body').show();
        }

        // Centered Logo in between nav items
        $('.primary-nav > li:eq(2)').after('<li class="menu-logo"><a href="/home"><img src="/app/themes/fleishers/dist/images/fleishers-logo.svg" alt="Fleishers Craft Butchery"></a></li>');
